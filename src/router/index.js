import { createRouter, createWebHashHistory } from "vue-router";

const routes = [
	{ // 首页
		path: '/',
		name: 'index',
		component: ()=>import('@/page/index/index.vue')
	},
	{ // 目的地
		path: '/found',
		name: 'found',
		component: ()=>import('@/page/found/found.vue')
	},
	{ // 发布游记
		path: '/publish',
		name: 'publish',
		component: ()=>import('@/page/publish/publish.vue')
	},
	{ // 我的
		path: '/mine',
		name: 'mine',
		component: ()=>import('@/page/mine/mine.vue')
	}
]

const router = createRouter({
	history: createWebHashHistory(),
	routes
})

export default router