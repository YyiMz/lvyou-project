import { createApp } from 'vue'
// import './style.css'
import App from './App.vue'

// 1. 引入你需要的组件
import { Button, Tabbar, TabbarItem, Search, Grid, GridItem } from 'vant';
// 2. 引入组件样式
import 'vant/lib/index.css';
import 'amfe-flexible'
// 路由
import router from '@/router/index.js'

const app = createApp(App)

// 3. 注册你需要的组件
app.use(Button);
app.use(Tabbar);
app.use(TabbarItem);
app.use(Search);
app.use(Grid);
app.use(GridItem);
app.use(router);
app.mount('#app')