import axios from 'axios'

const http = axios.create({
	baseURL: 'https://meituan.thexxdd.cn/apie', // 请求的公用api
	timeout: 10000 // 设置请求超时时间
})

// 设置相应拦截器: 请求发送之前的
http.interceptors.request.use((config) => {
	return config
}, (error) => {
	return Promise.reject(error)
})

// 相应拦截器: 请求发出之后的
http.interceptors.response.use((response)=>{
	// 接收正确结果: 状态码以2,3开头的
	return response.data
}, (error) => {
	// 接收错误结果: 状态码以4,5开头
	return Promise.reject(error)
})

// 导出请求方式: get，post
export default {
	get(url, params) {
		return http.get(url, { params })
	},
	post(url, data) {
		return http.post(url, data)
	}
}